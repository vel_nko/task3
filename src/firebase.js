import firebase from "firebase";

// firebase init - add your own config here
const firebaseConfig = {
  apiKey: "AIzaSyBN7Xq402DHJRkmw-rHz0bXpDQpEyUBaWE",
  authDomain: "test3-23e58.firebaseapp.com",
  projectId: "test3-23e58",
  storageBucket: "test3-23e58.appspot.com",
  messagingSenderId: "1086361867772",
  appId: "1:1086361867772:web:6c018bbfde2e8cd9cc8e79",
  measurementId: "G-TFZLP6LKYK",
};
export default firebase.initializeApp(firebaseConfig);
